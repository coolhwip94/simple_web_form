const models_selection = document.getElementById("models")
const radio_buttons = document.getElementsByName("fav_language")
const click_button = document.getElementById("click_button")
const get_coords_button = document.getElementById("get_coords")

const fname = document.getElementById("fname")
const lname = document.getElementById("lname")

const xcoord = document.getElementById("xcoord")
const ycoord = document.getElementById("ycoord")
const zcoord = document.getElementById("zcoord")
const bearing = document.getElementById("bearing")

const all_models = [
    "modela",
    "modelb",
    "modelc"
]
// adjusting dropdown
all_models.forEach(model => {
    let option = document.createElement("option")
    option.text = model
    models_selection.add(option)
});

// button click
click_button.onclick = (event) => {

    // log input fields
    console.log(fname.value, lname.value)

    console.log(xcoord.value, ycoord.value, zcoord.value)

    // log the dropdown selection model
    console.log(models_selection.value)

    // log the selected radio button value
    for (let index = 0; index < radio_buttons.length; index++) {
        if (radio_buttons[index].checked) {
            console.log(radio_buttons[index].value)
        }
    }
}

get_coords_button.onclick = (event) => {

    const coords = get_current_coords()
    
    xcoord.value = coords[0]
    ycoord.value = coords[1]
    zcoord.value = coords[2]
    bearing.value = coords[3]
}

const get_current_coords = () => {
    const coords = [0,1,2,3]
    return coords
}